This is the announcement as it was sent to
debian-devel-announce@lists.debian.org, October 13 2004. See also:
https://lists.debian.org/debian-devel-announce/2004/10/msg00007.html


Subject: Introducing the DebianGis subproject

DebianGis is a new sub-project recently launched thanks to the efforts of a
group of Debian developers and users. The goal of DebianGis is to create a
Custom Debian Distribution [1] oriented to serious Geographical Information
Systems (GIS) users and applications [2].

Debian already includes several GIS oriented programs and libraries, thus
our first aim is to coordinate development efforts. In addition, at this
stage we wish to collect as much information as possible on users' needs
and suggestions.

In the future we would broaden the project to include other Earth Observation
software categories in order to create the first all Free Software [3]
distribution dedicated to Remote Sensing, Earth Observation, and Geographical
Information Systems.

Of course, all people interested are invited to join us. We are currently
using the pkg-grass project site on Alioth [4] to provide a mailing list [5]
and wiki [6].

Stay tuned, more novelties would come as soon as possible...

*References:

[1] http://wiki.debian.net/index.cgi?CustomDebian
[2] http://en.wikipedia.org/wiki/Gis
[3] http://www.gnu.org/philosophy/free-sw.html
[4] https://alioth.debian.org/projects/pkg-grass/
[5] http://lists.alioth.debian.org/mailman/listinfo/pkg-grass-general
[6] http://pkg-grass.alioth.debian.org/cgi-bin/wiki.pl

--
The DebianGis Team
